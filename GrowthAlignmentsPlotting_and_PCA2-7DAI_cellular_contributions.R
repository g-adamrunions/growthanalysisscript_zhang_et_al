library(ggbiplot)
library(reshape2)

##Parameters and data-files used in plotting
plotPCA <- TRUE #turn on/off PCA plots
scale_PCA <- TRUE #toggle scaling option in prcomp
nbin <- 7 #number of bins in the growth alignments
poly_deg <-3 #degree of the polynomial to fit to the alignments

#Growth alignments to Load 
toLoad <- c("ZZ116wox012-7DAI_estimated_contribution_cell_ordinal.csv","ZZ116wox022-7DAI_estimated_contribution_cell_ordinal.csv","ZZ116wox032-7DAI_estimated_contribution_cell_ordinal.csv",
            "wox022-7DAI_estimated_contribution_cell_ordinal.csv","wox062-7DAI_estimated_contribution_cell_ordinal.csv","wox072-7DAI_estimated_contribution_cell_ordinal.csv",
            "WT022-7DAI_estimated_contribution_cell_ordinal.csv","WT032-7DAI_estimated_contribution_cell_ordinal.csv","WTCol032-7DAI_estimated_contribution_cell_ordinal.csv")
            
#Growth alignment to genotype mapping
genotypeMapping <- c("YUC1wox","YUC1wox","YUC1wox",
                     "wox","wox","wox",
                     "WT","WT","WT")

#Colors for plotting by genotype
colors <- c("wox"="dodgerblue4","WT"="forestgreen","YUC1wox"="orange")
colorsfill <- c("wox"="skyblue3","WT"="palegreen3","YUC1wox"="orange")

#Quantities to plot, and corresponding axis labels  
Plot_cols <- c("estCellCont")
Plot_labels <- c("Cellular contribution (%)")

##Start of actual analysis
##DATA SPECIFIC: portions of the code specific to this analysis
##INSPECTION POINT: commented out portions of the code that can be used to inspect the data/data analysis

GA_list <- list()
for(i in 1:length(toLoad)){
  GA_list[[i]] <- read.csv(toLoad[i])
}


for(col_name in Plot_cols)
{
 col_label <- Plot_labels[match(col_name,Plot_cols)]

 #Create a dataframe for PCA analysis
 PCAdf <- data.frame(matrix(NA, nrow=length(GA_list),ncol=nbin+1))
 names(PCAdf) = c(GA_list[[1]]$percentile,"Genotype")
 for(i in 1:length(GA_list)){
  PCAdf[i,]$Genotype <- genotypeMapping[i]

  for(j in 1:length(GA_list[[i]][,col_name])){
    PCAdf[i,j] <- GA_list[[i]][j,col_name]
  }
 }

  #Melt dataframe for plotting and fitting, process melted dataframe accordingly
  mPCAdf <- melt(PCAdf)
  mPCAdf$variable <- as.numeric(mPCAdf$variable)  
  
  #Convert bin-number to percentile
  mPCAdf$variable <- mPCAdf$variable/nbin*100

  mPCAdf <- mPCAdf[order(mPCAdf$Genotype,decreasing=TRUE),]
  mPCAdf$Genotype <- as.factor(mPCAdf$Genotype)
  
##INSPECTION POINT
#Here, we first check the fits of our least squares curve
#We're looking for a common model for all data without overfitting
#We want to make sure that all coefficients are required (i.e. significant) for at least one data-set
#  genotypes <- unique(mPCAdf$Genotype)
#  for(background in genotypes){
#    print(paste("Fitting summary for",col_label,"from",background))
#    cur_data <- mPCAdf[mPCAdf$Genotype == background,]
#    model <- lm(cur_data$value~poly(cur_data$variable,poly_deg))
#    print(summary(model))
#  }
  
  #Plot and save growth alignments by genotype, along with polynomial fitting curve
  p=ggplot(mPCAdf,aes(x=variable,y=value,color=Genotype))
  p=p+geom_smooth(alpha=0.25,aes(x=variable,y=value,fill=Genotype),se=TRUE,size=1.5,formula = y ~ poly(x,poly_deg),method="lm",level=0.95)+geom_point(alpha=0.5)
  p=p+scale_fill_manual(values = colorsfill)
  p=p+scale_color_manual(values = colors)
  p=p+coord_flip()+xlab("PD Percentile")+ylab(col_label)
  p=p+theme(text = element_text(size=20),axis.ticks.length = unit(1.5,"mm"),axis.ticks = element_line(color = "black",size=1.25),axis.line = element_line(color="black",size=1.25),panel.grid.major = element_blank(),panel.grid.minor = element_blank(),panel.background = element_rect(fill="white")) 
  plot(p)
  ggsave(plot=p, width=9,height=7,dpi=300,filename = paste(col_name,"fits.pdf",sep="_"));
  
  #Perform pca on growth alignments
  if(plotPCA){
    datacol <- length(PCAdf)-1 #First n-1 columns are Growth-alignment values, last column is genotype
    #Compute PCA using prcomp
    PCAdf.pca<-prcomp(PCAdf[,c(1:datacol)], center = TRUE, scale=scale_PCA)
    #Plot and save PCA plot of Growth alignments (colored by genotype)
    p=ggbiplot(PCAdf.pca,groups = PCAdf$Genotype,ellipse = TRUE,var.axes = FALSE)+geom_point(size=2,aes(color=PCAdf$Genotype))
    p=p+ggtitle(paste(col_label))+theme_minimal()
    p=p+scale_fill_manual(values = colorsfill)
    p=p+scale_color_manual(values = colors)
    p=p+theme(text = element_text(size=12),axis.ticks.length = unit(1.5,"mm"),axis.ticks = element_line(color = "black",size=1.0),axis.line = element_line(color="black",size=1.0))+labs(color="Genotype") 
    plot(p)
    ggsave(plot=p, width=6,height=5,dpi=300,filename = paste(col_name,"PCA.pdf",sep="_"));
  }
  
}
