This repository contains the R-Scripts and data required to reproduce the growth-alignment plots reported in Zhang et al.

File inventory:

MGXoutput: Output from MorphoGraphX. Clone data extracted for each sample are contained in CellData2-7.
Data files were obtained directly from MorphoGraphX, and contain values for each cell in the leaf-blade at
2 days after initiation as well as the corresponding clone emerging between 2-7 days after initiation. 
PD and ML aligned growth is contained in the CSV files containing "bezdir".

GrowthAlignmentGraphs2-7DAI.R: Performs growth alignments for all samples and outputs as CSV files.

GrowthAlignmentsPlotting_and_PCA2-7DAI.R: Loads the growth alignment CSV files and creates the plots shown
in Fig. 4J-K, N-P, R and Fig. S4J-K, N-P.

GrowthAlignmentsPlotting_and_PCA2-7DAI_cellular_contributions.R: Loads the growth alignment CSV files and 
creates the plots shown in Fig. 4M and Fig. S4M.

GrowthAlignmentsPlotting_and_PCA2-7DAI_area_contributions.R: Loads the growth alignment CSV files and 
creates the plots shown in Fig. 4L and Fig. S4L.
